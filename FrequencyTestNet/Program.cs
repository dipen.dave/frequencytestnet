using System;
using RestSharp;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Linq;
using System.Collections.Specialized;
using System.Collections.Concurrent;

namespace dotnetcore
{
    public enum FILETYPES
    {
        HundredKB = 1,
        FiveHundredKB,
        OneMB,
        TwoMB,
        FourMB,
        EightMB,
        SixteenMB,
        ThirtyTwoMB,
        FortyEightMB
    }
    public class Program
    {
        private static RestClient _ShipperDocUploadClient;
        public static RestClient ShipperDocUploadClient
        {
            get
            {
                if (_ShipperDocUploadClient == null)
                {
                    _ShipperDocUploadClient = new RestClient("https://app-dev-wse.flexe.com/s/dropoffs/container/6042/documents");
                    _ShipperDocUploadClient.Timeout = -1;
                }

                return _ShipperDocUploadClient;
            }
        }

        private static RestClient _WarehouseDocUploadClient;
        public static RestClient WarehouseDocUploadClient
        {
            get
            {
                if (_WarehouseDocUploadClient == null)
                {
                    _WarehouseDocUploadClient = new RestClient("https://app-dev-wse.flexe.com/api/v2/documents");
                    _WarehouseDocUploadClient.Timeout = -1;
                }

                return _WarehouseDocUploadClient;
            }
        }

        public static string ReportName
        {
            get
            {
                return $"../../../ReportsAndLogs/Report_{StartedAt}.csv";
            }
        }

        public static string LogName
        {
            get
            {
                return $"../../../ReportsAndLogs/Log_{StartedAt}.txt";
            }
        }

        public static ConcurrentQueue<string> LogQueue { get; set; }

        private static void WriteLogFile()
        {
            var message = "";
            var sb = new StringBuilder();
            while(LogQueue.TryDequeue(out message))
            {
                sb.AppendLine(message);
            }
            File.WriteAllText(LogName, sb.ToString());
        }

        public static string StartedAt { get; set; }

        static void Main(string[] args)
        {
            StartedAt = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            LogQueue = new ConcurrentQueue<string>();

            CreateTestFiles();

            var uploadFiles = Directory.EnumerateFiles("../../../TestUploadFiles/").ToList();
            var allreports = new List<Reportmetrics>();
            foreach (var file in uploadFiles)
            {
                for (int tps = 1; tps < 5; tps++)
                {
                    allreports.Add(ExecuteScenario(ShipperDocUploadClient, GetShipperRequest(file), 30, file, tps));
                }
            }

            File.WriteAllText(ReportName, Reportmetrics.GetReport(allreports));
            WriteLogFile();
        }

        static void CreateTestFiles()
        {
            var firstFileSizeInBytes = 1024 * 500;

            for (int i = 0; i < 5; i++)
            {
                CreateTestFile($"../../../TestUploadFiles/test_{firstFileSizeInBytes / 1024}KB.txt", firstFileSizeInBytes);
                firstFileSizeInBytes = firstFileSizeInBytes * 2;
            }
        }

        static void CreateTestFile(string filePath, int sizeInBytes)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < sizeInBytes/8; i++)
            {
                sb.Append("abcdefgh");
            }

            File.WriteAllText(filePath, sb.ToString());
        }

        private static void Log(string message)
        {
            Console.WriteLine($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff")}] {message}");
            LogQueue.Enqueue($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff")}] {message}");
        }

        private static RestRequest GetShipperRequest(string filePath)
        {
            var request = new RestRequest(Method.POST);
            request.AddHeader("cookie", "");
            request.AddHeader("x-csrf-token", "");
            request.AddParameter("authenticity_token", "");
            request.AddParameter("document[document_type]", "Bill Of Lading");
            request.AddParameter("document[description]", "Random String");
            request.AddFile("document[note]", filePath);

            return request;
        }

        private static RestRequest GetWarehouseRequest(string filePath)
        {
            var request = new RestRequest(Method.POST);
            request.AddHeader("Cookie", "");
            request.AddParameter("meta[correlationId]", "1337");
            request.AddParameter("data[notableId]", "1337");
            request.AddParameter("data[notableType]", "ContainerDelivery");
            request.AddParameter("data[documentType]", "Random String");
            request.AddFile("data[note]", filePath);
            return request;
        }

        static Reportmetrics ExecuteScenario(RestClient client, RestRequest request, int loadTime, string filePath, int tps)
        {
            Log($"Uploading {filePath} to {client.BaseUrl.ToString()} for {loadTime} seconds at {tps}tps/s");
            var requests = new ConcurrentBag<Task>();
            var responseTimes = new ConcurrentBag<int>();
            var isSuccessful = new ConcurrentBag<bool>();
            int totalRequests = 0;

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var timer = new Timer((e) =>
            {
                for (int i = 0; i < tps; i++)
                {
                    var startTicks = DateTime.Now.Ticks;
                    var result = client.ExecuteAsync(request);
                    result.ContinueWith(x =>
                    {
                        var responseTime = Convert.ToInt32((new TimeSpan(DateTime.Now.Ticks - startTicks)).TotalMilliseconds);
                        responseTimes.Add(responseTime);
                        isSuccessful.Add(x.Result.IsSuccessful);
                        Log($"ID: {x.Id} ResponseTime: {responseTime} Success: {x.Result.IsSuccessful} Error: {x.Result.ErrorMessage} StatusCode: {x.Result.StatusDescription}");
                        Interlocked.Increment(ref totalRequests);
                    });
                    requests.Add(result);
                };
            }, null, 0, 1000);


            Thread.Sleep(loadTime*1000);
            timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
            Log("Waiting for all the requests to finish...");
            var finalTask = Task.WhenAll(requests);
            finalTask.Wait();

            Log("Generating metrics...");
            var metrics = new Reportmetrics
            {
                CompletedAt = DateTime.Now.ToString(),
                Endpoint = client.BaseUrl.ToString(),
                FileSizeKB = Convert.ToInt32((new System.IO.FileInfo(filePath)).Length/1024),
                TransactionsPerSecond = tps,
                LoadTimeInSeconds = loadTime,
                ResponseTimesSeconds = new List<int>(responseTimes),
                IsSuccessful = new List<bool>(isSuccessful)
            };

            return metrics;
        }
    }

    public class Reportmetrics
    {
        public string CompletedAt { get; set; }
        public string Endpoint { get; set; }
        public int FileSizeKB { get; set; }
        public int TransactionsPerSecond { get; set; }
        public int LoadTimeInSeconds { get; set; }
        public List<int> ResponseTimesSeconds { get; set; }
        public List<bool> IsSuccessful { get; set; }
        private OrderedDictionary Metrics { get; set; }

        protected string GetHeader()
        {
            var sb = new StringBuilder();
            foreach (var key in Metrics.Keys)
            {
                sb.Append(key + ",");
            }
            var header = sb.ToString().TrimEnd(',');
            return header;
        }

        public static string GetReport(List<Reportmetrics> allMetrics)
        {

            var sb = new StringBuilder();
            var isHeaderSet = false;
            foreach (var metric in allMetrics)
            {
                metric.BuildMetrics();
                if (!isHeaderSet)
                {
                    sb.AppendLine(metric.GetHeader());
                    isHeaderSet = true;
                }
                var values = new String[metric.Metrics.Count];
                metric.Metrics.Values.CopyTo(values, 0);
                sb.AppendLine(String.Join(',', values));
            }

            return sb.ToString();
        }

        private int GetMedian(List<int> values)
        {
            int total = values.Count;
            var sorted = values.OrderBy(x => x).ToList();

            if (total % 2 == 0)
            {
                return (sorted[total / 2] + sorted[(total / 2) + 1]) / 2;
            }

            return sorted[(total / 2) + 1];
        }

        private void BuildMetrics()
        {
            Metrics = new OrderedDictionary();
            Metrics.Add("CompletedAt", CompletedAt);
            Metrics.Add("Endpoint", Endpoint);
            Metrics.Add("FileSize(KB)", FileSizeKB.ToString());
            Metrics.Add("Transactions per second", TransactionsPerSecond.ToString());
            Metrics.Add("Total load (seconds)", LoadTimeInSeconds.ToString());
            Metrics.Add("Total requests", ResponseTimesSeconds.Count.ToString());
            Metrics.Add("Max response time(ms)", ResponseTimesSeconds.Max().ToString());
            Metrics.Add("Min response time(ms)", ResponseTimesSeconds.Min().ToString());
            Metrics.Add("Mean response time(ms)", Math.Round(ResponseTimesSeconds.Average(), 2).ToString());
            Metrics.Add("Median response time(ms)", GetMedian(ResponseTimesSeconds).ToString());
            Metrics.Add("Success rate(%)", Math.Round(((IsSuccessful.Where(x => x == true).Count() * 100.0f) / IsSuccessful.Count()), 2).ToString());
        }
    }
}
